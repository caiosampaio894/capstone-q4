from functools import partial
from rest_framework import generics
from rest_framework.response import Response
from pokemon.models import Pokemon, TypeModel
from pokemon.serializers import PokemonsSerializer, TypesSerializer
from pokemon.permissions import IsAdminOrCommumUser, IsAdmin
from rest_framework.authentication import TokenAuthentication

class PokemonsView(generics.ListCreateAPIView):
	queryset = Pokemon.objects.all()
	serializer_class = PokemonsSerializer

	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAdminOrCommumUser]

	def create(self, request):
		serialized_data = self.get_serializer(data=request.data)
		serialized_data.is_valid(raise_exception=True)

		pokemon = serialized_data.save()
		
		types = request.data.get('types')
		
		if len(types) > 2:

			return Response({'error': 'type array greater than 2 lengths'})

		for type in types:
			
			type_name = type['name'].title()
			try:

				type_instance = TypeModel.objects.get(name=type_name)
				pokemon.types.add(type_instance)
			except:

				return Response({'error': 'type not found'})
		
		return Response(serialized_data.data)


class PokemonsViewById(generics.RetrieveUpdateDestroyAPIView):
	queryset = Pokemon.objects.all()
	serializer_class = PokemonsSerializer

	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAdminOrCommumUser]

	lookup_field = 'id'
	lookup_url_kwarg = 'pokemon_id'


	def partial_update(self, request, pokemon_id, **kwarg):
		pokemon_instance = Pokemon.objects.get(id=pokemon_id)
		serialized_data = self.get_serializer(
			pokemon_instance, data=request.data, partial=True
		)
		serialized_data.is_valid(raise_exception=True)
		serialized_data.save()

		types = request.data.pop('types')


		if len(types) > 2 or \
				(len(types) == 2 and \
					types[0].get('name') == types[1].get('name')):

			return Response({ 
				'error': 
				'type array is greater than 2 lengths or items are the same'
			})
		
		def add_types_to_instance():
			pokemon_instance.types.clear()

			for type in types:

				type_name = type['name'].title()
				try:

					type_instance = TypeModel.objects.get(name=type_name)
					print(type_instance)
					pokemon_instance.types.add(type_instance)
				except:

					return Response({'error': 'type not found'})
		add_types_to_instance()

		return Response(PokemonsSerializer(pokemon_instance).data)


class TypesView(generics.ListCreateAPIView):
	queryset = TypeModel.objects.all()
	serializer_class = TypesSerializer

	authentication_classes = [TokenAuthentication]
	permission_classes = [IsAdmin]