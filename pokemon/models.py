from django.db import models
from django.db import models

class TypeModel(models.Model):
    name = models.CharField(max_length=255, unique=True)

class Pokemon(models.Model):
    name = models.CharField(max_length=255)
    hp = models.IntegerField()
    atk = models.IntegerField()
    defense = models.IntegerField()
    spatk = models.IntegerField()
    spdefense = models.IntegerField()
    speed = models.IntegerField()
    types = models.ManyToManyField('TypeModel')
