from django.urls import path
from pokemon.views import PokemonsView, PokemonsViewById, TypesView

urlpatterns = [
	path('pokemons/', PokemonsView.as_view()),
	path('pokemons/<int:pokemon_id>/', PokemonsViewById.as_view()),
	path('types/', TypesView.as_view())
]