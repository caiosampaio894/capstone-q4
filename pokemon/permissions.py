from rest_framework.permissions import BasePermission
from rest_framework.permissions import SAFE_METHODS

class IsAdminOrCommumUser(BasePermission):
	def has_permission(self, request, view):
		
		if request.method in SAFE_METHODS:
			return True

		is_superuser = request.user.is_superuser

		if is_superuser:
			return True
		
		return False

class IsAdmin(BasePermission):
	def has_permission(self, request, view):
		
		is_superuser = request.user.is_superuser

		if is_superuser:
			return True
		
		return False