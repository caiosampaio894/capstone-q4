from rest_framework.serializers import ModelSerializer
from pokemon.models import Pokemon, TypeModel
from rest_framework import serializers
from django.http import Http404

class TypesSerializer(serializers.Serializer):
	name = serializers.CharField()

	def create(self, validated_data):
		pokemon = TypeModel.objects.create(**validated_data)
		return pokemon


class PokemonsSerializer(ModelSerializer):
	types = TypesSerializer(many=True, read_only=True)
	
	class Meta:
		model = Pokemon
		fields = '__all__'
		depth = 1


