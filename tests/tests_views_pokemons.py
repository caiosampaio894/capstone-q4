from django.test import TestCase
from rest_framework.test import APIClient


class PokemonViewTest(TestCase):
    def setUp(self):
        
        self.client = APIClient()
        
        self.admin_data = {"username": "admin", "password": "1234", "is_superuser": True}
        
        self.admin_login_data = {"username": "admin", "password": "1234"}
        
        self.url_accounts = f"/api/accounts/"
        self.url_login = f"/api/login/"
        
        self.url_types = f"/api/types/"
        self.url_pokemon = f"/api/pokemons/"
        self.url_pokemon_update = f"/api/pokemons/1/"
        self.url_pokemon_delete = f"/api/pokemons/1/"
        
        self.types_data = {"name": "Eletric"}
        
        self.pokemon_data = {
                "name": "raichu",
                "types": [
                    {
                        "name": "Eletric"
                    }
                ],
                "hp": 90,
                "atk": 90,
                "defense": 80,
                "spatk": 100,
                "spdefense": 80,
                "speed": 100
            }
        
        self.response_create_pokemon = {
            "id": 1,
            "types": [
                {
                    "name": "Eletric"
                }
            ],
            "name": "raichu",
            "hp": 90,
            "atk": 90,
            "defense": 80,
            "spatk": 100,
            "spdefense": 80,
            "speed": 100
            }
        
        self.response_list_pokemon = [
                {
                "id": 1,
                "types": [
                    {
                    "name": "Eletric"
                    }
                ],
                "name": "raichu",
                "hp": 90,
                "atk": 90,
                "defense": 80,
                "spatk": 100,
                "spdefense": 80,
                "speed": 100
                }
            ]

        self.pokemon_update_data = {
                "name": "pikachu",
                "types": [
                    {
                        "name": "eletric"
                    }
                ],
                "hp": 90,
                "atk": 90,
                "defense": 80,
                "spatk": 100,
                "spdefense": 80,
                "speed": 100
            }
        
        self.response_update_pokemon = {
                "id": 1,
                "types": [
                    {
                    "name": "Eletric"
                    }
                ],
                "name": "pikachu",
                "hp": 90,
                "atk": 90,
                "defense": 80,
                "spatk": 100,
                "spdefense": 80,
                "speed": 100
            }
        
    def test_create_pokemon(self):

        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_types, self.types_data)
        
        response = self.client.post(self.url_pokemon, self.pokemon_data, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.response_create_pokemon)
        
    def test_list_pokemon(self):
               
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_types, self.types_data, format="json")
        
        self.client.post(self.url_pokemon, self.pokemon_data, format="json")
        
        response = self.client.get(self.url_pokemon, format="json")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.response_list_pokemon)
        
    def test_update_pokemon(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_types, self.types_data)
        
        response = self.client.post(self.url_pokemon, self.pokemon_data, format="json")
        
        response = self.client.put(self.url_pokemon_update, self.pokemon_update_data, format="json")

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.response_update_pokemon)
        
    def test_delete_pokemon(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        self.client.post(self.url_types, self.types_data)
        
        response = self.client.post(self.url_pokemon, self.pokemon_data, format="json")
        
        response = self.client.delete(self.url_pokemon_delete)
        
        self.assertEqual(response.status_code, 204)
