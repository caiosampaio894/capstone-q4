from django.test import TestCase
from rest_framework.test import APIClient


class TrainerViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        
        self.admin_data = {"username": "admin", "password": "1234", "is_superuser": True}
        
        self.trainer_data = {"username": "trainer", "password": "1234", "is_superuser": False}
        
        self.admin_login_data = {"username": "admin", "password": "1234"}
        
        self.trainer_login_data = {"username": "trainer", "password": "1234"}
        
        self.admin_login_fail_data = {"username": "admin", "password": "12345"}
        
        self.url_accounts = f"/api/accounts/"
        self.url_login = f"/api/login/"
        self.url_list_users = f"/api/trainer/"
        
        self.url_filter_list_users = f"/api/trainer/1/"
        self.url_delete_trainer = f"/api/trainer/1/"
        self.url_update_trainer = f"/api/trainer/1/"
        
        self.response_create_user = {
                "id": 1,
                "username": "admin",
                "is_superuser": True
            }
        
        self.response_list_users = [
            {
                "id": 1,
                "username": "admin",
                "is_superuser": True
            }
        ]
        
        self.response_filter_user = {"id": 1, "username": "admin" }
        
        self.update_user_data = {"username": "admin2", "is_superuser": False}
        self.response_update_trainer = {"id": 1,"username": "admin2", "is_superuser": False}
        
    def test_create_user(self):
  
        response = self.client.post(self.url_accounts, self.admin_data)
        self.assertEqual(response.status_code, 201)
        
    def test_repeat_user_creation(self):
        self.client.post(self.url_accounts, self.trainer_data, format="json")

        response = self.client.post(self.url_accounts, self.trainer_data, format="json")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"username": ["A user with that username already exists."]})
        
    def test_login_success(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        response = self.client.post(self.url_login, self.admin_login_data, format="json")
        self.assertEqual(response.status_code, 200)
        
        # retorno do token
        self.assertIn('token', response.json())
    
    def test_login_fail(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        response = self.client.post(self.url_login, self.admin_login_fail_data)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json(), {"error": "Invalid credentials"})
        
    def test_list_users(self):        
        self.client.post(self.url_accounts, self.admin_data, format="json")
        
        response = self.client.get(self.url_list_users, format="json")
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.response_list_users)
        
    def test_filter_users_sucess(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        response = self.client.get(self.url_filter_list_users)
        
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), self.response_filter_user)
        
    def test_filter_users_fail(self):
        
        self.client.post(self.url_accounts, self.admin_data)
        
        response = self.client.get("/api/trainer/99/")
        
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {"detail": "Not found."})
        
    def test_delete_a_trainer_without_permission(self):
        self.client.post(self.url_accounts, self.admin_data)
        self.client.post(self.url_accounts, self.trainer_data)
        
        token = self.client.post(self.url_login, self.trainer_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.delete(self.url_delete_trainer)
        
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {"detail": "You do not have permission to perform this action."})

    def test_delete_a_trainer(self):
        self.client.post(self.url_accounts, self.trainer_data)
        self.client.post(self.url_accounts, self.admin_data)
        
        token = self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.delete(self.url_delete_trainer)
        
        self.assertEqual(response.status_code, 204)
        
    def test_update_a_trainer(self):

        self.client.post(self.url_accounts, self.admin_data)

        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.put(self.url_update_trainer, self.update_user_data)
        
        self.assertEqual(response.status_code, 200)
        
        self.assertEqual(response.json(), self.response_update_trainer)
        
    def test_update_a_trainer_fail(self):
       
        self.client.post(self.url_accounts, self.admin_data)

        token =self.client.post(self.url_login, self.admin_login_data).json()['token']
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)
        
        response = self.client.put("/api/trainer/2/", self.update_user_data)
        
        self.assertEqual(response.status_code, 403)        
        self.assertEqual(response.json(), {"detail": "You do not have permission to perform this action."})