# Pokeside API

A aplicação tem como objetivo construir uma API para retratar um jogo onde pessoas (`trainers`) podem cadastrar, listar, atualizar e deletar pokemons. De modo a possibilitar o cadastro e visualização de todos os pokemons, seus tipos e atributos, formando assim um grande banco de dados para amantes da franquia Pokemon.

### URL base da api no heroku:
https://pokeside-capstone-q4.herokuapp.com/

## Como instalar e rodar pelo localhost? 🚀

Para instalar o sistema, é necessário seguir alguns passos, como baixar o projeto e fazer instalação das dependências. Para isso, é necessário abrir uma aba do terminal e digitar o seguinte:

`# Este passo é para baixar o projeto`

```bash
git clone https://gitlab.com/<your_user>/e-4-generic-views-kmdb.git
```

Depois que terminar de baixar, é necessário entrar na pasta, criar um ambiente virtual e entrar nele:

`# Entrar na pasta`

```bash
cd e2-kanvas
```

`# Criar um ambiente virtual`

python3 -m venv venv

`# Entrar no ambiente virtual`

```bash
source venv/bin/activate
```

Então, para instalar as dependências, basta:

```bash
pip install -r requirements.txt
```

Depois de ter instalado as dependências, é necessário conficurar o arquivo `.env` e rodar as migrações para que o banco de dados e as tabelas sejam transformadas:

```bash
./manage.py migrate
```

Então, para rodar, basta digitar o seguinte, sem terminal:

```bash
./manage.py runserver
```

E o sistema estará rodando em http://127.0.0.1:8000/

## Utilização 🖥

Para usar este sistema, é necessário usar um API Client, como o Insomnia.

## Rotas:

### Sobre Trainer:

**POST** api/accounts/

Cria um novo usuário. (`"trainer"`)

Body:

```json
{
    "username": "ian",
    "password": "1234",
    "is_superuser": false
} 

```
Response:

```json
// RESPONSE STATUS -> HTTP 201
{
  "id": 2,
  "username": "ian",
  "is_superuser": false
}
```


### Sobre Autenticação:

A API funcionará com autenticação baseada em token.

**POST** /api/login/

fazendo login (serve para qualquer tipo de usuário):

Body:

```json
{
    "username": "student",
    "password": "1234"
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 200
{
    "token": "dfd384673e9127213de6116ca33257ce4aa203cf"
}
```

### Sobre Pokemon:

**POST** /api/pokemons/


Esse endpoint só poderá ser acessado por um usuário do tipo admin, e é responsável por criar novos pokemons.


Body:

```json
// Header -> Authorization: Token <token-do-admin>
{
	"name":"Pichu",
	"hp":100,
	"atk":10,
	"defense":20,
	"spatk":50,
	"spdefense":30,
	"speed":50,
	"types":[{"name": "eletric"}]
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 201 CREATED
{ 
  "id": 1,
  "types": [
    {
      "name": "Eletric"
    }
  ],
  "name": "pichu",
  "hp": 100,
  "atk": 10,
  "defense": 20,
  "spatk": 50,
  "spdefense": 30,
  "speed": 50
}
```

`* Observação:`

    Não é possível criar um novo type de    pokemon pelo endpoint então para usar em seu `localhost` deve inserir manualmente no banco de dados usando o arquivo queries.sql.

#### Os Types são:

```
[
    "Bug",
    "Dark",
    "Dragon",
    "Eletric",
    "Fairy",
    "Fighting",
    "Fire",
    "Flying",
    "Ghost",
    "Grass",
    "Ground",
    "Ice",
    "Normal",
    "Poison",
    "Psychic",
    "Rock",
    "Steel",
    "Wate"'
]
```


**PUT** /api/pokemons/\<int:pokemon_id>

Esse endpoint só poderá ser acessado por um usuário do tipo admin, e é responsável por Alterar os dados do pokemon.

Body: 

```json
// Header -> Authorization: Token <token-do-admin>
{
	"name":"pichu",
	"hp":60,
	"atk":5,
	"defense":10,
	"spatk":30,
	"spdefense":20,
	"speed":50,
	"types":[{"name": "eletric"}]
}
```

```json
// RESPONSE STATUS -> HTTP 201 CREATED
{
  "id": 1,
  "types": [
    {
      "name": "Eletric"
    }
  ],
  "name": "pichu",
  "hp": 60,
  "atk": 5,
  "defense": 10,
  "spatk": 30,
  "spdefense": 20,
  "speed": 50
}
```


**GET** /api/pokemons/

Este endpoint pode ser acessado por qualquer cliente, mesmo que não seja autenticado. A resposta do servidor será uma lista dos pokemons que existem.


Resposta:

```json
// RESPONSE STATUS -> 200 OK
[
  {
    "id": 1,
    "types": [
      {
        "name": "Eletric"
      }
    ],
    "name": "pichu",
    "hp": 60,
    "atk": 5,
    "defense": 10,
    "spatk": 30,
    "spdefense": 20,
    "speed": 50
  },
  {
    "id": 2,
    "types": [
      {
        "name": "Eletric"
      }
    ],
    "name": "picachu",
    "hp": 100,
    "atk": 10,
    "defense": 20,
    "spatk": 50,
    "spdefense": 30,
    "speed": 50
  },
  {
    "id": 3,
    "types": [
      {
        "name": "Fire"
      }
    ],
    "name": "charmander",
    "hp": 100,
    "atk": 10,
    "defense": 20,
    "spatk": 50,
    "spdefense": 30,
    "speed": 50
  }
]
```


**GET** /api/pokemons/\<int:pokemon_id>/

Este endpoint pode ser acessado por qualquer client (mesmo sem autenticação). A resposta do servidor é o pokemon filtrado pelo pokemon_id.


```json
// RESPONSE STATUS -> HTTP 200 OK
{
  "id": 3,
  "types": [
    {
      "name": "Fire"
    }
  ],
  "name": "charmander",
  "hp": 100,
  "atk": 10,
  "defense": 20,
  "spatk": 50,
  "spdefense": 30,
  "speed": 50
}
```


**DELETE** /api/pokemons/\<int:pokemon_id>/

Somente um usuário do tipo admin poderá deletar o pokemon.

Se for possível deletar irá retornar um status `HTTP 204 - No Content`


### Sobre Pokemons Trainer:

**POST** /api/\<int:pokemon_id>/pokemons_trainer/

Somente um trainer pode capturar um pokemon.

Body:

```json
// Header -> Authorization: Token <token>
// Opcional
{
	"pokemon_nick": "bateria3"
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 201 CREATED
{
  "id": 1,
  "pokemon": {
    "id": 3,
    "types": [
      {
        "name": "Fire"
      }
    ],
    "name": "charmander",
    "hp": 100,
    "atk": 10,
    "defense": 20,
    "spatk": 50,
    "spdefense": 30,
    "speed": 50
  },
  "pokemon_nick": "bateria3"
}
```

**GET** /api/pokemons_trainer/

Este endpoint lista todos os pokemons do trainer.


Body:

```json
// Header -> Authorization: Token <token>

```

Response:

```json
// RESPONSE STATUS -> HTTP 200 ok
[
  {
    "id": 1,
    "pokemon": {
      "id": 1,
      "types": [
        {
          "name": "Fire"
        },
        {
          "name": "Flying"
        }
      ],
      "name": "charmander",
      "hp": 120,
      "atk": 10,
      "defense": 20,
      "spatk": 50,
      "spdefense": 30,
      "speed": 50
    },
    "pokemon_nick": "bateria3"
  },
  {
    "id": 2,
    "pokemon": {
      "id": 1,
      "types": [
        {
          "name": "Fire"
        },
        {
          "name": "Flying"
        }
      ],
      "name": "charmander",
      "hp": 120,
      "atk": 10,
      "defense": 20,
      "spatk": 50,
      "spdefense": 30,
      "speed": 50
    },
    "pokemon_nick": "bateria2"
  },
  {
    "id": 3,
    "pokemon": {
      "id": 1,
      "types": [
        {
          "name": "Fire"
        },
        {
          "name": "Flying"
        }
      ],
      "name": "charmander",
      "hp": 120,
      "atk": 10,
      "defense": 20,
      "spatk": 50,
      "spdefense": 30,
      "speed": 50
    },
    "pokemon_nick": "bateria1"
  },
  {
    "id": 4,
    "pokemon": {
      "id": 1,
      "types": [
        {
          "name": "Fire"
        },
        {
          "name": "Flying"
        }
      ],
      "name": "charmander",
      "hp": 120,
      "atk": 10,
      "defense": 20,
      "spatk": 50,
      "spdefense": 30,
      "speed": 50
    },
    "pokemon_nick": null
  }
]
```


**GET** /api/pokemons_trainer/\<int:pokemons_trainer_id>/

Este endpoint retorna um pokemons do trainer filtrado pelo `trainer_id`.

Body:

```json
// Header -> Authorization: Token <token>

```

Response:

```json
// RESPONSE STATUS -> HTTP 200 ok
{
  "id": 1,
  "pokemon": {
    "id": 1,
    "types": [
      {
        "name": "Fire"
      },
      {
        "name": "Flying"
      }
    ],
    "name": "charmander",
    "hp": 120,
    "atk": 10,
    "defense": 20,
    "spatk": 50,
    "spdefense": 30,
    "speed": 50
  },
  "pokemon_nick": "bateria3"
}
```

**PATCH** /api/pokemons_trainer/\<int:pokemons_trainer_id>/

Este endpoint permite que altere o apelido de seu pokemon.


Body:

```json
// Header -> Authorization: Token <token>
{
  "pokemon_nick": "edgy"
}

```

Response:

```json
// RESPONSE STATUS -> HTTP 200 ok
{
  "id": 1,
  "pokemon": {
    "id": 1,
    "types": [
      {
        "name": "Fire"
      },
      {
        "name": "Flying"
      }
    ],
    "name": "charmander",
    "hp": 120,
    "atk": 10,
    "defense": 20,
    "spatk": 50,
    "spdefense": 30,
    "speed": 50
  },
  "pokemon_nick": "edgy"
}
```


**DELETE** /api/pokemons_trainer/\<int:pokemons_trainer_id>/

Permite o trainer solte um de seus pokemons (`deleta um pokemon da lista de pokemons do trainer`).

Se for possível deletar irá retornar um status `HTTP 204 - No Content`


### Sobre Team:

**PUT** api/team/

Permite criar ou alterar seu time de pokemons que irá usar em batalhas.

`*Observação: Seu time pode ter no máximo 6 pokemons e não pode ter pokemons de outros trainer.`

Body:

```json
// Header -> Authorization: Token <token>
{
	"team_ids":[2,3]
}

```

Response:

```json
// RESPONSE STATUS -> HTTP 200 ok
{
  "trainer": {
    "id": 1,
    "username": "adimin"
  },
  "team": [
    {
      "id": 5,
      "pokemon": {
        "id": 2,
        "pokemon": {
          "id": 1,
          "types": [
            {
              "name": "Fire"
            },
            {
              "name": "Flying"
            }
          ],
          "name": "charmander",
          "hp": 120,
          "atk": 10,
          "defense": 20,
          "spatk": 50,
          "spdefense": 30,
          "speed": 50
        },
        "pokemon_nick": "bateria2"
      }
    },
    {
      "id": 6,
      "pokemon": {
        "id": 3,
        "pokemon": {
          "id": 1,
          "types": [
            {
              "name": "Fire"
            },
            {
              "name": "Flying"
            }
          ],
          "name": "charmander",
          "hp": 120,
          "atk": 10,
          "defense": 20,
          "spatk": 50,
          "spdefense": 30,
          "speed": 50
        },
        "pokemon_nick": "bateria1"
      }
    }
  ]
}
```

### Sobre Moves:

**POST** /api/moves/


Esse endpoint só poderá ser acessado por um usuário do tipo admin, e é responsável por criar novos movimentos pokemon.


Body:

```json
// Header -> Authorization: Token <token-do-admin>
{
	"name": "Thundershok",
	"basepower": 85,
	"pp": 30,
	"description": "Pikachu-exclusive Z-Move. High critical hit ratio.",
	"category": "special",
	"type": {"name": "Eletric"}
}
```

Response:

```json
// RESPONSE STATUS -> HTTP 201 CREATED
{
  "id": 3,
  "type": {
    "name": "Eletric"
  },
  "name": "Thundershok",
  "basepower": 85,
  "pp": 30,
  "description": "Pikachu-exclusive Z-Move. High critical hit ratio.",
  "category": "special"
}
```


**GET** /api/moves/

Este endpoint pode ser acessado por qualquer cliente, `mesmo que não seja autenticado`. A resposta do servidor será uma lista dos movimentos que existem.


Resposta:

```json
// RESPONSE STATUS -> 200 OK
[
  {
    "id": 2,
    "type": {
      "name": "Eletric"
    },
    "name": "Thunderbolt",
    "basepower": 195,
    "pp": 1,
    "description": "Pikachu-exclusive Z-Move. High critical hit ratio.",
    "category": "special"
  },
  {
    "id": 3,
    "type": {
      "name": "Eletric"
    },
    "name": "Thundershok",
    "basepower": 85,
    "pp": 30,
    "description": "Pikachu-exclusive Z-Move. High critical hit ratio.",
    "category": "special"
  }
]
```


**GET** /api/moves/\<int:move_id>/

Este endpoint pode ser acessado por qualquer client, `mesmo sem autenticação`. A resposta do servidor é um movimento filtrado pelo `move_id`.


```json
// RESPONSE STATUS -> HTTP 200 OK
{
  "id": 2,
  "type": {
    "name": "Eletric"
  },
  "name": "Thunderbolt",
  "basepower": 195,
  "pp": 1,
  "description": "Pikachu-exclusive Z-Move. High critical hit ratio.",
  "category": "special"
}
```


**DELETE** /api/moves/\<int:move_id>/

Somente um usuário do tipo admin poderá deletar o movimento pokemon.

Se for possível deletar irá retornar um status `HTTP 204 - No Content`


```python
"""
NOTE: Nesta versão do projeto ainda não é possível ensinar os moves para os seus pokemons. 
"""
```
