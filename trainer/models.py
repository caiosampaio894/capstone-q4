from typing import Match
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.deletion import CASCADE
from pokemon.models import Pokemon, TypeModel


class Trainer(AbstractUser):
    pass


class Moves(models.Model):
    name = models.CharField(max_length=255, unique=True)
    basepower = models.IntegerField()
    pp = models.IntegerField()
    description = models.TextField()    
    category = models.CharField(max_length=255)
    type = models.ForeignKey(TypeModel, related_name='moves', on_delete=CASCADE)


class PokemonsTrainer(models.Model):
    moves = models.ManyToManyField('Moves')
    pokemon = models.ForeignKey(Pokemon, on_delete=CASCADE)
    pokemon_nick = models.CharField(max_length=255, null=True)
    trainer = models.ForeignKey('Trainer', related_name='pokemons_trainer', on_delete=CASCADE)


class TeamsModel(models.Model):
	trainer = models.ForeignKey(Trainer, null=False, related_name='team', \
		on_delete=models.CASCADE)
	pokemon = models.ForeignKey(PokemonsTrainer, null=False, related_name='team', \
		on_delete=models.CASCADE)
