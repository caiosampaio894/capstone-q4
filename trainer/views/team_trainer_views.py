from django.shortcuts import get_object_or_404, get_list_or_404
from rest_framework import viewsets, status
import rest_framework
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from django.db.utils import IntegrityError
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication

from django.core.exceptions import ObjectDoesNotExist
from rest_framework.serializers import Serializer

from ..models import  TeamsModel, Trainer, PokemonsTrainer
from ..serializers.trainer_serializer import AccountSerializer, LoginSerializer, TrainerSerializer
from ..serializers.team_trainer_serializes import TeamsSerializer
from ..permissions.trainer_permissions import TrainerPermission


class TeamTrainerView(viewsets.ViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]

    lookup_url_kwarg = 'trainer_id'

    def edit_team(self, request):
        try:
            team_ids = request.data['team_ids']
            trainer: Trainer = request.user

            if len(team_ids) > 6:
                return Response({'error': 'team cannot have more than 6 pokemons'}, 400)

            current_team = TeamsModel.objects.filter(trainer_id=trainer.id)
            
            if len(current_team) > 0:
                for obj in current_team:
                    
                    obj.delete()
            
            for i in team_ids:

                try:
                    pokemon_trainer = PokemonsTrainer.objects.get(id=i)

                    if not pokemon_trainer.trainer == trainer:
                        return Response({'errors': f'You are not pokemon_id {i} trainer'}, 404)
                    
                    dict_poke_of_team = {
                        "trainer": trainer,
                        "pokemon": pokemon_trainer
                    }

                    TeamsModel.objects.create(**dict_poke_of_team)

                except ObjectDoesNotExist:
                    return Response({'detail': f"Do not exist pokemon_trainer with id {i}"}, 404)

            team = get_list_or_404(TeamsModel, trainer_id = trainer.id)
            
            serializer = TeamsSerializer(team, many=True)

            response = {
                "trainer": TrainerSerializer(trainer).data,
                "team": serializer.data
            }
            return Response(response, 200)
            
        except TypeError as e:
            return Response({'errors': str(e)}, 400)


    def list (self, request, **kwargs):

        trainer = Trainer.objects.get(id=int(kwargs['trainer_id']))
        
        team = TeamsModel.objects.filter(trainer_id = trainer.id)
        
        serializer = TeamsSerializer(team, many=True)

        response = {
                "trainer": TrainerSerializer(trainer).data,
                "team": serializer.data
            }

        return Response(response, 200)
