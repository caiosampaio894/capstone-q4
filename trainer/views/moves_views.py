from rest_framework.generics import get_object_or_404
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework import status
from trainer.models import Moves
from trainer.permissions.trainer_permissions import IsAdminPermission
from trainer.serializers.moves_serializer import MoveSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticatedOrReadOnly


class MovesViewSet(ViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly, IsAdminPermission]

    lookup_url_kwarg = 'moves_id'


    def create(self, request):
        data = request.data
        serializer = MoveSerializer(data=data)

        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


    def list(self, request):
        moves = Moves.objects.all()
        serializer = MoveSerializer(moves, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


    def retrieve(self, request, *args, **kwargs):
        moves_id = kwargs.get('moves_id')
        moves = get_object_or_404(Moves, id=moves_id)

        serializer = MoveSerializer(moves)

        return Response(serializer.data, status=status.HTTP_200_OK)


    def update(self, request, *args, **kwargs):
        data=request.data
        moves_id = kwargs.get('moves_id')
        moves = Moves.objects.filter(id=moves_id).first()

        serializer = MoveSerializer(moves, data=data)
     
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_200_OK)
      

    def destroy(self, request, *args, **kwargs):
        moves_id = kwargs.get('moves_id')
        moves = get_object_or_404(Moves, id=moves_id)

        moves.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)