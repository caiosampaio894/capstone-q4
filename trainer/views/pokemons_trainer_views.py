from typing import Generic
from django import http
from django.db.models import query
from django.http import response
from django.shortcuts import render
from rest_framework.generics import  ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from trainer.models import PokemonsTrainer
from trainer.serializers.pokemons_trainer_serializer import PokemonsTrainerSerializer
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
import ipdb
from pokemon.models import Pokemon

class PokemonsTrainerView(ListCreateAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = PokemonsTrainer.objects.all()
    serializer_class = PokemonsTrainerSerializer
    lookup_url_kwarg = 'pokemon_id'
    
    def get_queryset(self):
        return self.request.user.pokemons_trainer
        

    

class PokemonsTrainerDetailView(RetrieveUpdateDestroyAPIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = PokemonsTrainer.objects.all()
    serializer_class = PokemonsTrainerSerializer
    lookup_url_kwarg = 'pokemon_trainer_id'

    def get(self, request, *args, **kwargs):
        pokemons_trainer = PokemonsTrainer.objects.filter(id=kwargs['pokemon_trainer_id']).first()
        serializer = PokemonsTrainerSerializer(pokemons_trainer)
        return Response(serializer.data, status=200)
