from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
import rest_framework
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from django.contrib.auth import authenticate
from django.db.utils import IntegrityError
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication
from ..models import Trainer
from ..serializers.trainer_serializer import AccountSerializer, LoginSerializer, TrainerSerializer
from ..permissions.trainer_permissions import TrainerPermission


class TrainerViewSet(viewsets.ViewSet):
    authentication_classes = [TokenAuthentication]
    permission_classes = [TrainerPermission]

    lookup_url_kwarg = 'trainer_id'
    
    
    def accounts(self, request):
        try:
            serializer = AccountSerializer(data=request.data)

            serializer.is_valid(raise_exception=True)

            serializer.save()

            return Response(serializer.data, 201)
        except IntegrityError:
            return Response({
                "username": [
                    "A user with that username already exists."
                ]
            }, 400)
    

    def login(self, request):
        serializer = LoginSerializer(data=request.data)
        
        serializer.is_valid(raise_exception=True)

        user = authenticate(**serializer.validated_data)

        if user:
            token = Token.objects.get_or_create(user=user)[0]
            return Response({'token': token.key}, 200)

        return Response({'error': 'Invalid credentials'}, 401)

    def list (self, request):
        trainers = Trainer.objects.all()
        serializer = AccountSerializer(trainers, many=True)
        return Response(serializer.data, 200)

    
    def retrieve(self, request, *args, **kwargs):
        trainer = get_object_or_404(Trainer, pk=kwargs['trainer_id'])
        
        serializer = TrainerSerializer(trainer)
        
        return Response(serializer.data, 200)


    def destroy(self, request, *args, **kwargs):
        trainer = get_object_or_404(Trainer, pk=kwargs['trainer_id'])
        
        trainer.delete()
        
        return Response('', 204)

    
    def update(self, request, *args, **kwargs):
        try:
            data = request.data

            trainer = get_object_or_404(Trainer, pk=kwargs['trainer_id'])

            serializer = AccountSerializer(trainer)

            serializer.update(trainer, data)

            return Response(serializer.data, 200)
        except IntegrityError as  e:
            return Response({
                "username": [
                    "A user with that username already exists."
                ]
            }, 400)
    
