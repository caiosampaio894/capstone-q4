from rest_framework import serializers
import ipdb
from pokemon.models import Pokemon
from trainer.models import PokemonsTrainer
from trainer.serializers.trainer_serializer import TrainerSerializer
from pokemon.serializers import PokemonsSerializer

class PokemonsTrainerSerializer(serializers.ModelSerializer):
    # trainer = TrainerSerializer()
    pokemon = PokemonsSerializer(required=False)
    class Meta:
        model = PokemonsTrainer
        fields = ['id', 'pokemon', 'pokemon_nick']
        extra_kwargs = {'pokemon_nick': {'required': False}}

    def create(self, validated_data):
        trainer = self.context['request'].user
        pokemon = Pokemon.objects.get(id=self.context['view'].kwargs['pokemon_id'])
        validated_data['pokemon'] = pokemon
        validated_data['trainer'] = trainer
        var = PokemonsTrainer.objects.create(**validated_data)
        return var

