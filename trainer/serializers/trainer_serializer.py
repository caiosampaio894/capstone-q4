from django.db.models import fields
from rest_framework import serializers
from ..models import Trainer

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trainer
        fields = ['id', 'username', 'password', 'is_superuser']

        extra_kwargs= {'password':{'write_only': True}}

    def create(self, validated_data):
        return Trainer.objects.create_user(**validated_data)



class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField(write_only=True)


class TrainerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trainer
        fields = ['id', 'username']