from rest_framework import serializers
from ..models import TeamsModel
from ..serializers.trainer_serializer import TrainerSerializer
from ..serializers.pokemons_trainer_serializer import PokemonsTrainerSerializer

class TeamsSerializer(serializers.ModelSerializer):
	pokemon = PokemonsTrainerSerializer()
	class Meta:
		model = TeamsModel
		fields = '__all__'
		
		extra_kwargs={'trainer': {'write_only': True}}

	def create(self, validated_data):
		user_id = self.user.id
		user_team_list = TeamsModel.objects.filter(user_id=user_id)

		if user_team_list >= 6:
			raise

		new_member = TeamsModel.objects.create(**validated_data)
		return new_member