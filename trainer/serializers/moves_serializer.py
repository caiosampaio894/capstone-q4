from rest_framework.serializers import ModelSerializer
from pokemon.models import TypeModel
from trainer.models import Moves
from pokemon.serializers import TypesSerializer


class MoveSerializer(ModelSerializer):
    type = TypesSerializer(required=False)

    class Meta:
        model = Moves
        fields = '__all__'
        depth = 1

    def create(self, validated_data):
        type = validated_data.pop('type')
        type_instance = TypeModel.objects.get(name=type["name"])
        validated_data["type"] = type_instance

        moves = Moves.objects.create(**validated_data)
        
        return moves


    def update(self, instance, validated_data):
        type = validated_data.pop('type')
        type_instance = TypeModel.objects.get(name=type["name"])

        validated_data["type"] = type_instance

        return super().update(instance, validated_data)
        

        

        
    
    