from django.contrib import admin
from trainer.views.pokemons_trainer_views import PokemonsTrainerDetailView, PokemonsTrainerView
from rest_framework.routers import SimpleRouter
from django.urls import path, include
from .views.team_trainer_views import TeamTrainerView
from trainer.views.trainer_views import TrainerViewSet
from .views.moves_views import MovesViewSet
 
router = SimpleRouter()
router.register(prefix=r'trainer', viewset=TrainerViewSet, basename='trainer')
router.register(prefix='moves', viewset=MovesViewSet, basename='moves')
# router.register(prefix='team', viewset=TeamTrainerView, basename='team')

urlpatterns = [
    path('accounts/', TrainerViewSet.as_view({'post': 'accounts'})),
    path('login/', TrainerViewSet.as_view({'post': 'login'})),
    path('team/', TeamTrainerView.as_view({'put': 'edit_team'})),
    path('team/<int:trainer_id>/', TeamTrainerView.as_view({'get': 'list'})),
    path('trainer/', TrainerViewSet.as_view({'get': 'list'})),
    path('<int:pokemon_id>/pokemons_trainer/', PokemonsTrainerView.as_view()),
    path('pokemons_trainer/', PokemonsTrainerView.as_view()),
    path('pokemons_trainer/<int:pokemon_trainer_id>/', PokemonsTrainerDetailView.as_view()),
    path('', include(router.urls))
]
