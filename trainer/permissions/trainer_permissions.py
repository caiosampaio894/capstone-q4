from rest_framework.permissions import BasePermission
from django.contrib.auth.models import AnonymousUser

class TrainerPermission(BasePermission):
    def has_permission(self, request, view):
        
        if bool(request.method == 'GET') or bool(request.method == 'POST'):
            return True
        
        user = request.user

        trainer_id = view.kwargs['trainer_id']
        
        is_permission = bool(bool(user.id == int(trainer_id)) or bool(user.is_superuser and bool(request.method == 'DELETE')))

        return is_permission


class IsAdminPermission(BasePermission):
    def has_permission(self, request, view):
        user = request.user

        if bool(user.is_superuser) or bool(request.method == 'GET'):
            return True

        
