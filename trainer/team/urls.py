from django.urls import path
from .view import TeamView

urlpatterns = [
	path('team/', TeamView.as_view())
]