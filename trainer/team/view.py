from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.response import Response

from team.models import TeamsModel
from team.serializer import TeamsSerializer

class TeamView(RetrieveUpdateAPIView):
	queryset = TeamsModel.objects.all()
	serializer_class = TeamsSerializer

	# method uncompleted
	def retrive(self, request):
		user_id = request.user.id
		user_team = TeamsModel.objects.filter(trainer_id=user_id)

		instance = self.get_serializer(user_team)

		return Response(instance.data)

	# method uncompleted
	def update(self, request):
		user_id = request.user.id
		
		try:
			pokemons = {**request.data}
		except: 
			return Response({'error': 'has same pokemons in your team'})

		if pokemons > 6:
			return Response({'error': 'you have more than six pokemons in array'})

		for pokemon in pokemons:
			serialized_data = self.get_serializer(trainer=user_id, pokemon=pokemon)
			serialized_data.is_valid(raise_exception=True)

			serialized_data.save()

		return Response(serialized_data.data)